# Jodel-Wetterfrosch 🐸
[![Code Health](https://landscape.io/github/wetterfroschdus/Jodel-Wetterfrosch/master/landscape.svg?style=flat)](https://landscape.io/github/wetterfroschdus/Jodel-Wetterfrosch/master)                                                                 
![Karma Hamburg](https://rawgit.com/wetterfroschdus/karma-badges/master/hamburg.svg) ![Karma Bremen](https://rawgit.com/wetterfroschdus/karma-badges/master/bremen.svg) ![Karma Düsseldorf](https://rawgit.com/wetterfroschdus/karma-badges/master/dusseldorf.svg) ![Karma Dortmund](https://rawgit.com/wetterfroschdus/karma-badges/master/dortmund.svg) ![Karma Mönchengladbach](https://rawgit.com/wetterfroschdus/karma-badges/master/mgladbach.svg) ![Karma Essen](https://rawgit.com/wetterfroschdus/karma-badges/master/essen.svg)


## How to use
- Clone the repository
```shell
git clone https://gitlab.com/wetterfroschdus/Jodel-Wetterfrosch.git
```

- Install the requirements: [jodel_api](https://github.com/nborrmann/jodel_api/) and [dateutil](https://dateutil.readthedocs.io/en/stable/)
```shell
pip install jodel_api python-dateutil
```
#### [jodel_api](https://github.com/nborrmann/jodel_api/) by [nborrmann](https://github.com/nborrmann) is not updated with new HMAC keys from new Jodel versions anymore, so it won't work. There's still a way to get the keys, just google around a bit. 😉 

- Get an [API Key](https://developer.accuweather.com/packages) from Accuweather

- Use create_account.py to generate the necessary data:
```shell
python create_account.py
```
 Just follow the instructions 😉

## Run it!
Use jodel_wetterfrosch.py to create a weather Jodel:
```shell
python jodel_wetterfrosch.py -a account_file.json
```
If you place the account_file in a different folder as the script, you need to specify the full path to it:
```shell
python jodel_wetterfrosch.py -a /foo/bar/account_file.json
```








### Weather Data Provider
<a href="https://www.accuweather.com" target="_blank"><img src="https://www.dropbox.com/sh/l06xfikm0y7pfpa/AAAP1K6zf4_O8V8VLv83Mzo-a/AW_Primary_Logos/Horizontal_Logos/PNG/RGB/AW_RGB.png?raw=1" 
alt="Accuweather Logo" height="60" border="0" /></a>

### Pollen Data Provider
<a href="https://www.dwd.de"><img src="https://upload.wikimedia.org/wikipedia/de/thumb/7/7b/DWD-Logo_2013.svg/800px-DWD-Logo_2013.svg.png" 
alt="DWD Logo" height="70" border="0" /></a>
